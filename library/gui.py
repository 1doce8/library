# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../UI/gui.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(806, 604)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(450, 130, 161, 22))
        self.comboBox.setObjectName("comboBox")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(250, 180, 541, 311))
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setWordWrap(True)
        self.tableWidget.setCornerButtonEnabled(True)
        self.tableWidget.setRowCount(10)
        self.tableWidget.setColumnCount(10)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.horizontalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setVisible(False)
        self.make_order = QtWidgets.QLabel(self.centralwidget)
        self.make_order.setGeometry(QtCore.QRect(80, 90, 101, 16))
        self.make_order.setObjectName("make_order")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 120, 47, 13))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 150, 47, 13))
        self.label_3.setObjectName("label_3")
        self.add = QtWidgets.QPushButton(self.centralwidget)
        self.add.setGeometry(QtCore.QRect(90, 210, 75, 23))
        self.add.setObjectName("add")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(620, 130, 111, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(10, 180, 71, 16))
        self.label_5.setObjectName("label_5")
        self.load_all_btn = QtWidgets.QPushButton(self.centralwidget)
        self.load_all_btn.setGeometry(QtCore.QRect(420, 500, 91, 23))
        self.load_all_btn.setObjectName("load_all_btn")
        self.clear_database_btn = QtWidgets.QPushButton(self.centralwidget)
        self.clear_database_btn.setGeometry(QtCore.QRect(594, 500, 111, 23))
        self.clear_database_btn.setObjectName("clear_database_btn")
        self.master_selector = QtWidgets.QComboBox(self.centralwidget)
        self.master_selector.setGeometry(QtCore.QRect(80, 120, 111, 22))
        self.master_selector.setObjectName("master_selector")
        self.client_selector = QtWidgets.QComboBox(self.centralwidget)
        self.client_selector.setGeometry(QtCore.QRect(80, 150, 111, 22))
        self.client_selector.setObjectName("client_selector")
        self.repair_selector = QtWidgets.QComboBox(self.centralwidget)
        self.repair_selector.setGeometry(QtCore.QRect(80, 180, 111, 22))
        self.repair_selector.setObjectName("repair_selector")
        self.masters = QtWidgets.QLabel(self.centralwidget)
        self.masters.setGeometry(QtCore.QRect(450, 110, 151, 16))
        self.masters.setObjectName("masters")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(390, 160, 81, 16))
        self.label.setObjectName("label")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(500, 160, 81, 16))
        self.label_4.setObjectName("label_4")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.master_selector.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.make_order.setText(_translate("MainWindow", "Оформить задание"))
        self.label_2.setText(_translate("MainWindow", "Мастер"))
        self.label_3.setText(_translate("MainWindow", "Клиент"))
        self.add.setText(_translate("MainWindow", "Оформить"))
        self.pushButton_2.setText(_translate("MainWindow", "Загрузить"))
        self.label_5.setText(_translate("MainWindow", "Вид ремонта"))
        self.load_all_btn.setText(_translate("MainWindow", "Выгрузить всё"))
        self.clear_database_btn.setText(_translate("MainWindow", "Очистить таблицу"))
        self.masters.setText(_translate("MainWindow", "Выберите мастера"))
        self.label.setText(_translate("MainWindow", "Номер заказа"))
        self.label_4.setText(_translate("MainWindow", "Номер заказа"))
