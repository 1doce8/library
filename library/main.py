import pymysql
import configparser
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import datetime
from ui import Ui_MainWindow

global hid
hid = 1

if __name__ == "__main__":
    # create app
    app = QtWidgets.QApplication(sys.argv)

    # create form and init ui
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()


# Hook logic
def db_connect():
    config = configparser.ConfigParser()
    config.read("settings.ini")
    try:
        connection = pymysql.connect(host=config["MYSQL"]["host"],
                                     user=config["MYSQL"]["user"],
                                     password=config["MYSQL"]["password"],
                                     db=config["MYSQL"]["dataBase"],
                                     autocommit=True)

    except Exception:
        connected = False
    else:
        connected = True
    finally:
        return connection, connected


def insert_new(book, buyer, count):
    try:
        sql = "INSERT INTO orders(id_author, id_book, count) VALUES('" + book + "', '" + buyer + "', '" + count + "')"
        execute_sql(sql)

    except Exception:
        result = False

    else:
        result = True

    finally:
        return result


def execute_sql(sql):
    connection, connected = db_connect()
    if connected:
        with connection.cursor() as cursor:
            try:
                cursor.execute(sql)
                result = cursor.fetchall()
                return result

            except Exception:
                result = False

            else:
                result = True

            finally:
                cursor.close()
                return result


def truncate_table():
    sql = "TRUNCATE orders"
    res = execute_sql(sql)
    fill_table_all()
    message_box('success', 'Таблица успешно отчищена')


if __name__ == "__main__":

    def message_box(title, message):
        mess = QtWidgets.QMessageBox()
        mess.setWindowTitle(title)
        mess.setText(message)
        mess.setStandardButtons(QtWidgets.QMessageBox.Ok)
        mess.exec_()


    def add_new():
        buyer = str(ui.master_selector.currentIndex() + 1)
        book = str(ui.client_selector.currentIndex() + 1)
        count = ui.lineEdit.text()
        #repair = str(ui.repair_selector.currentIndex() + 1)
        insert_new(book, buyer, count)
        fill_selectors()
        fill_table_all()
        message_box('success', 'Заявка добавлена')


    # def add_new_user():
    #     book = ui.name_input.text()
    #     buyer = ui.surname_input.text()
    #     count = str(ui.state_selector.currentIndex())
    #     telephone = ui.number_input.text()
    #     car = ui.car_input.text()


    # def get_interval():
    #     date_start = ui.date_start.text()
    #     date_end = ui.date_end.text()
    #     sql = "SELECT * FROM test WHERE time BETWEEN '" + date_start + "' AND '" + date_end + "'"
    #     result = execute_sql(sql)
    #     ui.tableWidget.setRowCount(0)
    #
    #     for row_number, row_data in enumerate(result):
    #         ui.tableWidget.insertRow(row_number)
    #         for column_number, data in enumerate(row_data):
    #             ui.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
    #
    # ui.pushButton_2.clicked.connect(get_interval)

    def get_masters():
        sql = "SELECT name FROM book"
        data = execute_sql(sql)
        ui.master_selector.clear()
        comboBox = [row[0] for row in data]
        for row in comboBox:
            ui.master_selector.addItem(row)


    def get_clients():
        sql = "SELECT name, surname FROM author"
        data = execute_sql(sql)
        ui.client_selector.clear()
        comboBox = [row[0] + ' ' + row[1] for row in data]
        for row in comboBox:
            ui.client_selector.addItem(row)


    # def get_repair():
    #     sql = "SELECT name FROM repair_type"
    #     data = execute_sql(sql)
    #     ui.repair_selector.clear()
    #     comboBox = [row[0] for row in data]
    #     for row in comboBox:
    #         ui.repair_selector.addItem(row)

    def fill_selectors():
        get_masters()
        get_clients()
        # get_repair()


    def fill_table(sql):
        result = execute_sql(sql)
        ui.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            ui.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                ui.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))


    def fill_table_all():
        sql = "SELECT orders.id, author.name, author.surname, book.name, orders.count " \
              "FROM orders " \
              "LEFT JOIN author ON orders.id_author = author.id " \
              "LEFT JOIN book ON orders.id_book = book.id " \
                "ORDER BY id ASC"
        fill_table(sql)

    def hide():
        global hid
        if hid == 1:
            ui.make_order.hide()
            ui.label_2.hide()
            ui.label_3.hide()
            ui.label_5.hide()
            ui.master_selector.hide()
            ui.client_selector.hide()
            ui.lineEdit.hide()
            ui.add.hide()
            hid = 0
        else:
            ui.make_order.show()
            ui.label_2.show()
            ui.label_3.show()
            ui.label_5.show()
            ui.master_selector.show()
            ui.client_selector.show()
            ui.lineEdit.show()
            ui.add.show()
            hid = 1

    def fill_table_master():
        sql = "SELECT orders.id, author.name, author.surname, book.name, orders.count " \
              "FROM orders " \
              "LEFT JOIN author ON orders.id_author = author.id " \
              "LEFT JOIN book ON orders.id_book = book.id " \
              "ORDER BY orders.count DESC"
        print(sql)
        fill_table(sql)
    message_box('success', 'Данные загружены')

    fill_table_all()
    fill_selectors()
    hide()

    ui.pushButton.clicked.connect(hide)

    ui.add.clicked.connect(add_new)
    ui.load_all_btn.clicked.connect(fill_table_all)

    ui.clear_database_btn.clicked.connect(truncate_table)

    ui.sort_top_btn.clicked.connect(fill_table_master)

    # ui.load_master_orders.clicked.connect(fill_table_master)

    # ui.state.clear()
    # ui.state.insertItem(0, 'False')
    # ui.state.insertItem(1, 'True')

    # Run main loop
    sys.exit(app.exec_())
