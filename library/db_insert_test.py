from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import pymysql
import configparser
from ui import Ui_MainWindow
import unittest


class ProgramTestCase(unittest.TestCase):
    """ Тест для main.py """

    def test_insert_into_table(self):
        # create app
        app = QtWidgets.QApplication(sys.argv)

        # create form and init ui
        MainWindow = QtWidgets.QMainWindow()
        ui = Ui_MainWindow()
        ui.setupUi(MainWindow)
        MainWindow.show()

        from main import db_connect, insert_new, execute_sql

        master = str(ui.master_selector.currentIndex()+2)
        client = str(ui.client_selector.currentIndex()+2)
        repair = str(ui.repair_selector.currentIndex()+2)

        result = insert_new(master, client, repair)
        self.assertEqual(result, True)

if __name__ == "__main__":
    unittest.main()


